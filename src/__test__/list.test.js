import { render, screen } from "@testing-library/react";

import List from "../components/List";

test("Should show teste when prop have it", () => {
  render(<List textList={["teste"]} />);

  const fromScreen = screen.getByText("teste");

  expect(fromScreen).toBeInTheDocument();
});

test("Should show all the strings separately in the prop", () => {
  render(<List textList={["teste", "1", "45"]} />);

  const lastElement = screen.queryByText("45");
  const secondElement = screen.queryByText("1");

  expect(lastElement).toBeInTheDocument();
  expect(secondElement).toBeInTheDocument();
});
