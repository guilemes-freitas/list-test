import { fireEvent, render, screen } from "@testing-library/react";

import Form from "../components/Form";
import List from "../components/List";

test("Should add the new value to textList", () => {
  render(
    <>
      <Form /> <List textList={[]} />
    </>
  );
  const input = screen.getByPlaceholderText("Escreva o que deseja adicionar");
  const button = screen.getByText("Adicionar");

  fireEvent.change(input, { target: { value: "teste1" } });

  fireEvent(
    button,
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
    })
  );

  const fromScreen = screen.getByText("teste1");

  expect(fromScreen).toBeInTheDocument();
});

test("Should disable button when input is empty", () => {
  render(<Form />);
  const input = screen.getByPlaceholderText("Escreva o que deseja adicionar");
  const button = screen.getByRole("button");

  fireEvent.change(input, { target: { value: "teste" } });
  expect(button).not.toBeDisabled();

  fireEvent.change(input, { target: { value: "" } });
  expect(button).toBeDisabled();
});

test("Should disable button when first entering", () => {
  render(<Form />);
  const button = screen.getByRole("button");

  expect(button).toBeDisabled();
});
