interface ListProps {
  textList: string[];
}

const List = ({ textList }: ListProps) => {
  return (
    <ul>
      {textList.map((text, index) => {
        return <li key={index}>{text}</li>;
      })}
    </ul>
  );
};

export default List;
