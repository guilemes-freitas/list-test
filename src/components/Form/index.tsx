import { useState } from "react";
import List from "../List";

export const Form = () => {
  const [inputValue, setInputValue] = useState<string>("");
  const [lock, setLock] = useState<boolean>(true);
  const [textList, setTextList] = useState<string[]>([] as string[]);
  const handleLock = (inputValue: string) => {
    setLock(!!!inputValue);
  };
  return (
    <>
      <input
        onChange={(e) => {
          setInputValue(e.target.value);
          handleLock(e.target.value);
        }}
        placeholder="Escreva o que deseja adicionar"
      ></input>
      <button
        onClick={() => {
          setTextList([...textList, inputValue]);
        }}
        disabled={lock}
      >
        Adicionar
      </button>
      <List textList={textList}></List>
    </>
  );
};

export default Form;
